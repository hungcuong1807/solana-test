import { ALICE, BOB, BOB_ACCOUNT, connection, PAYER_ACCOUNT } from "./constants";
import { Transaction, SystemProgram } from "@solana/web3.js"

(async () => {
    let tx = new Transaction().add(
        SystemProgram.transfer({
            fromPubkey: BOB,
            toPubkey: ALICE,
            lamports: 1e7, // 1 SOL
        })
    );
    tx.feePayer = PAYER_ACCOUNT.publicKey;

    let txhash = await connection.sendTransaction(tx, [PAYER_ACCOUNT, BOB_ACCOUNT]);
    console.log(`txhash: ${BOB_ACCOUNT.publicKey.toBase58()}`);
})();