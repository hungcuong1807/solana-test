import { PublicKey, Transaction } from "@solana/web3.js";

import { ALICE, BOB, BOB_ACCOUNT, connection, PAYER_ACCOUNT, } from "./constants";

import * as SPLToken from "@solana/spl-token";

// token transfer

async function main() {
    try {
        let tx = new Transaction();
        tx.add(
            SPLToken.Token.createTransferInstruction(
                SPLToken.TOKEN_PROGRAM_ID, // always token program address
                new PublicKey("7H1kiXSXWPHfL7h9L1fKpLzX2Px4mjmS7np7kVRTcaus"),
                new PublicKey("CZpd9gNvNnbww4q8z2c1QxjefyoaU4igJ9xsEGpLbnuT"), // from (token account public key)
                // to (token account public key)
                BOB, // from's authority
                [], // pass signer if from's mint is a multisig pubkey
                1 // amount 
            )
        );
        tx.feePayer = PAYER_ACCOUNT.publicKey;
        await connection.sendTransaction(tx, [BOB_ACCOUNT, PAYER_ACCOUNT]);
    }
    catch (err) {
        console.log("error", err.logs);
    }

}

main().then(
    () => process.exit(),
    (err) => {
        console.error(err);
        process.exit(-1);
    }
);