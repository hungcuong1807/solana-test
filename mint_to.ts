import { Transaction, PublicKey } from "@solana/web3.js";
import { connection, MINT_ADDRESS, MIN_ADDRESS_2, PAYER_ACCOUNT } from "./constants";
import * as SPLToken from "@solana/spl-token";

// mint token 
async function main() {
    let tx = new Transaction();
    tx.add(
        SPLToken.Token.createMintToInstruction(
            SPLToken.TOKEN_PROGRAM_ID, // always token program id
            MIN_ADDRESS_2, // mint
            new PublicKey("CJhvyhvwfiAExkGXAgSvoTuZym2cGo5umi6Sjvd1kRax"), // receiver (also need a token account)
            PAYER_ACCOUNT.publicKey, // mint's authority
            [], // if mint's authority is a multisig account, then we pass singers into it, for now is empty
            10 // mint amount, you can pass whatever you want, but it is the smallest unit, so if your decimals is 9, you will need to pass 1e9 to get 1 token
        )
    );
    tx.feePayer = PAYER_ACCOUNT.publicKey;
    console.log(PAYER_ACCOUNT.publicKey)

    console.log(`txhash: ${await connection.sendTransaction(tx, [PAYER_ACCOUNT])}`);
}

main().then(
    () => process.exit(),
    (err) => {
        console.error(err);
        process.exit(-1);
    }
);

