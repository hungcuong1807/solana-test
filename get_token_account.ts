import { connection, ALICE, } from "./constants";
import * as SPLToken from "@solana/spl-token";
import { PublicKey } from "@solana/web3.js";
import * as BufferLayout from 'buffer-layout';

export const ACCOUNT_LAYOUT = BufferLayout.struct([
  BufferLayout.blob(32, 'mint'),
  BufferLayout.blob(32, 'owner'),
  BufferLayout.nu64('amount'),
  BufferLayout.blob(93),
]);

function getOwnedAccountsFilters(publicKey: PublicKey) {
  return [
    {
      memcmp: {
        offset: ACCOUNT_LAYOUT.offsetOf('owner'),
        bytes: publicKey.toBase58(),
      },
    },
    {
      dataSize: ACCOUNT_LAYOUT.span,
    },
  ];
}

async function main() {
  let tokenAccountPubkey = new PublicKey(
    "GfgZN2Nim5fbFhVJJpasAApsVzVrYzMmwKzNwjR3i58X"
  );

  let tokenAccount = await new SPLToken.Token(
    connection,
    null,
    SPLToken.TOKEN_PROGRAM_ID,
    null
  ).getAccountInfo(ALICE);
  console.log(tokenAccount);
}

main().then(
  () => process.exit(),
  (err) => {
    console.error(err);
    process.exit(-1);
  }
);
