// need to create a token account to receice token in Solana
// if you want to receive USDC, you will need a USDC token account

// we need to use mint address to create a token account with our wallet

import { connection, MINT_ADDRESS, PAYER_ACCOUNT, ALICE, MIN_ADDRESS_2, BOB } from "./constants";
import * as SPLToken from "@solana/spl-token";
import { Keypair, Transaction, SystemProgram } from "@solana/web3.js";

async function main() {
    // 2. ATA

    // you always get the same address if you pass the same mint and token account owner
    let ata = await SPLToken.Token.getAssociatedTokenAddress(
        SPLToken.ASSOCIATED_TOKEN_PROGRAM_ID, // always associated token program id
        SPLToken.TOKEN_PROGRAM_ID, // always token program id
        MIN_ADDRESS_2, // mint
        BOB // token account authority
    );
    console.log(`ata: ${ata.toBase58()}`);
    console.log(MINT_ADDRESS.toBase58())

    let ataTx = new Transaction();
    ataTx.add(
        SPLToken.Token.createAssociatedTokenAccountInstruction(
            SPLToken.ASSOCIATED_TOKEN_PROGRAM_ID, // always associated token program id
            SPLToken.TOKEN_PROGRAM_ID, // always token program id
            MIN_ADDRESS_2, // mint (which we used to calculate ata)
            ata, // the ata we calcualted early
            BOB, // token account owner (which we used to calculate ata)
            PAYER_ACCOUNT.publicKey // payer, fund account, like SystemProgram.createAccount's from
        )
    );
    ataTx.feePayer = PAYER_ACCOUNT.publicKey;

    console.log(`ata txhash: ${await connection.sendTransaction(ataTx, [PAYER_ACCOUNT])}`);
}

main().then(
    () => process.exit(),
    (err) => {
        console.error(err);
        process.exit(-1);
    }
);