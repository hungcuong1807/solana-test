import { connection } from "./constants";
import { PublicKey } from "@solana/web3.js"

// get token balance

async function main() {
    console.log(await connection.getTokenAccountBalance(new PublicKey("CZpd9gNvNnbww4q8z2c1QxjefyoaU4igJ9xsEGpLbnuT")));
}

main().then(
    () => process.exit(),
    (err) => {
        console.error(err);
        process.exit(-1);
    }
);