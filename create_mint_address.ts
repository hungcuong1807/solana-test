import { Keypair, Transaction, SystemProgram, LAMPORTS_PER_SOL } from "@solana/web3.js";

import { connection, PAYER_ACCOUNT, MINT_ADDRESS } from "./constants";

import * as SPLToken from "@solana/spl-token";

// create mint (create your own token)

// you can treat mint as ERC-20's token address in Ethereum
// so, SRM, RAY, USDC... all of them are mint

async function main() {
    // create a mint account
    let mint = Keypair.generate();
    console.log(`mint: ${mint.publicKey.toBase58()}`);

    let tx = new Transaction();
    tx.add(
        // create account
        SystemProgram.createAccount({
            fromPubkey: PAYER_ACCOUNT.publicKey,
            newAccountPubkey: mint.publicKey,
            space: SPLToken.MintLayout.span,
            lamports: LAMPORTS_PER_SOL,
            programId: SPLToken.TOKEN_PROGRAM_ID,
        }),
        // init mint
        SPLToken.Token.createInitMintInstruction(
            SPLToken.TOKEN_PROGRAM_ID, // program id, always token program id
            mint.publicKey, // mint account public key
            0, // decimals
            PAYER_ACCOUNT.publicKey, // mint authority (an auth to mint token)
            null // freeze authority (we use null first, the auth can let you freeze user's token account)
        )
    );
    tx.feePayer = PAYER_ACCOUNT.publicKey;

    let txhash = await connection.sendTransaction(tx, [mint, PAYER_ACCOUNT]);
    console.log(`txhash: ${txhash}`);
}

main().then(
    () => process.exit(),
    (err) => {
        console.error(err);
        process.exit(-1);
    }
);
SPLToken.Token