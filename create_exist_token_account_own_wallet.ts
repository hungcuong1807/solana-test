import { connection, MINT_ADDRESS, PAYER_ACCOUNT, ALICE, USDT_TOKEN_ADDRESS } from "./constants";
import * as SPLToken from "@solana/spl-token";
import { Keypair, Transaction, SystemProgram } from "@solana/web3.js";
import { TokenListProvider, TokenInfo } from '@solana/spl-token-registry';
import * as bs58 from "bs58";


async function main() {
    const usdt_token = new SPLToken.Token(connection, USDT_TOKEN_ADDRESS, SPLToken.TOKEN_PROGRAM_ID, null);
    const usdt_token_account_with_wallet = await usdt_token.getOrCreateAssociatedAccountInfo(ALICE);


    console.log(usdt_token_account_with_wallet);
    console.log("aaa")
    // new TokenListProvider().resolve().then((tokens) => {

    //     const tokenList = tokens.filterByClusterSlug('mainnet-beta').getList();
    //     console.log(tokenList);
    // });
}
main().then(
    () => process.exit(),
    (err) => {
        console.error(err);
        process.exit(-1);
    }
);