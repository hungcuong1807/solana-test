import { PublicKey } from "@solana/web3.js";
import { ALICE, BOB_ACCOUNT, connection, MINT_ADDRESS, PAYER_ACCOUNT } from "./constants";
import * as SPLToken from "@solana/spl-token";
import * as BufferLayout from 'buffer-layout';


export const ACCOUNT_LAYOUT = BufferLayout.struct([
  BufferLayout.blob(32, 'mint'),
  BufferLayout.blob(32, 'owner'),
  BufferLayout.nu64('amount'),
  BufferLayout.blob(93),
]);
export function getOwnedAccountsFilters(publicKey: PublicKey) {
  return [
    {
      memcmp: {
        offset: ACCOUNT_LAYOUT.offsetOf('owner'),
        bytes: publicKey.toBase58(),
      },
    },
    {
      dataSize: ACCOUNT_LAYOUT.span,
    },
  ]
}
(async () => {
  // const token_accounts = await connection.getTokenLargestAccounts(MINT_ADDRESS);
  const token_account = await connection.getTokenAccountsByOwner(BOB_ACCOUNT.publicKey, { programId: SPLToken.TOKEN_PROGRAM_ID });

  // console.log(token_accounts)
  token_account.value.forEach(token => console.log(token.pubkey.toBase58()))
})();
