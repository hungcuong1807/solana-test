import { Keypair, Connection, PublicKey, clusterApiUrl } from "@solana/web3.js";
import * as bip39 from "bip39";

export const API_ENDPOINT = clusterApiUrl("devnet");
export const connection = new Connection(API_ENDPOINT, 'confirmed');

export const ALICE = new PublicKey(
  "2jyFyeXf1gqSbmaYUYhy3dsnwCjQ5Q6se8WgodkPLdVu"
);

export const BOB = new PublicKey("E3xLE9TpVDvJAt5QxHJHo113vGG3Q3cFgfPMGXKDVUxB");


const mnemonic = "hat girl energy remember produce tooth column admit envelope eyebrow zone plate";
const seed = bip39.mnemonicToSeedSync(mnemonic, "password");
export const PAYER_ACCOUNT = Keypair.fromSeed(seed.slice(0, 32));

const mnemonic_bob = "more valley limit ripple burger small mask jungle panther grain science injury";
const seed_bob = bip39.mnemonicToSeedSync(mnemonic_bob, "password");
export const BOB_ACCOUNT = Keypair.fromSeed(seed_bob.slice(0, 32));
const mnemonic_mint = "whip announce wasp urge frog draft ritual ginger power regret buyer hover";
const seed_mint = bip39.mnemonicToSeedSync(mnemonic_mint, "");
export const MINT_ADDRESS = new PublicKey("E4ZN2KmnVmpwLwjJNAwRjuQLeE5iFHLcAJ8LGB7FMaGQ");
export const MIN_ADDRESS_2 = new PublicKey("2GFPqSqRMTrw4TfpW8aqWcdXVVRKjpZN3pPs1w7bbrF3")

export const USDT_TOKEN_ADDRESS = new PublicKey("3MoHgE6bJ2Ak1tEvTt5SVgSN2oXiwt6Gk5s6wbBxdmmN")

