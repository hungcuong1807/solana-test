import { Keypair, LAMPORTS_PER_SOL } from "@solana/web3.js";
import { API_ENDPOINT, connection, PAYER_ACCOUNT } from "./constants";

async function main() {
  console.log(`Requesting airdrop funds... (this will take 30 seconds)`);

  let airdropSignature = await connection.requestAirdrop(
    PAYER_ACCOUNT.publicKey,
    LAMPORTS_PER_SOL * 2
  );
  await connection.confirmTransaction(airdropSignature);
  const lamports = await connection.getBalance(PAYER_ACCOUNT.publicKey);
  console.log(`payer account public key: ${PAYER_ACCOUNT.publicKey.toBase58()}`);
  console.log(
    `Payer account ${PAYER_ACCOUNT.publicKey.toBase58()} containing ${(
      lamports / 1000000000
    ).toFixed(2)}SOL`
  );
}

main().then(
  () => process.exit(),
  (err) => {
    console.error(err);
    process.exit(-1);
  }
);
