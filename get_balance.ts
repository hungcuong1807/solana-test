import { Connection, Keypair, LAMPORTS_PER_SOL } from "@solana/web3.js";
import { ALICE, connection, PAYER_ACCOUNT } from "./constants";
// connection
(async () => {
    let balance = await connection.getBalance(ALICE);
    console.log(`${balance / LAMPORTS_PER_SOL} SOL`);
    let accountInfo = await connection.getAccountInfo(ALICE);
    console.log(accountInfo);
})();