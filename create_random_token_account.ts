import { Keypair, Transaction, SystemProgram } from "@solana/web3.js"
import { connection, MINT_ADDRESS, PAYER_ACCOUNT, ALICE } from "./constants";
import * as SPLToken from "@solana/spl-token";
(async () => {
    // 1. Random
    let randomTokenAccount = Keypair.generate();
    console.log(`ramdom token address: ${randomTokenAccount.publicKey.toBase58()}`);

    let randomTokenAccountTx = new Transaction();
    randomTokenAccountTx.add(
        // create account
        SystemProgram.createAccount({
            fromPubkey: PAYER_ACCOUNT.publicKey,
            newAccountPubkey: randomTokenAccount.publicKey,
            space: SPLToken.AccountLayout.span,
            lamports: await SPLToken.Token.getMinBalanceRentForExemptAccount(connection),
            programId: SPLToken.TOKEN_PROGRAM_ID,
        }),
        // init token account
        SPLToken.Token.createInitAccountInstruction(
            SPLToken.TOKEN_PROGRAM_ID, // program id, always token program id
            MINT_ADDRESS, // mint
            randomTokenAccount.publicKey, // token account public key
            ALICE // token account authority
        )
    );
    randomTokenAccountTx.feePayer = PAYER_ACCOUNT.publicKey;

    console.log(
        `random token account txhash: ${await connection.sendTransaction(randomTokenAccountTx, [
            randomTokenAccount,
            PAYER_ACCOUNT,
        ])}`
    );
})()